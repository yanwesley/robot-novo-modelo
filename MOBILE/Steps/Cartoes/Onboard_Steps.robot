*** Settings ***
Resource    ../../PageObjects/Cartoes/AjusteCartoes_Page.robot
Resource    ../../Steps/Cartoes/Token_Steps.robot
Resource    ../../Steps/Cartoes/Home_Catoes_Steps.robot
Resource    ../../../Utils/Utils.robot
Resource    ../../PageObjects/Cartoes/Onboard_Page.robot
Variables   ../../Screens/Cartoes/OnBoard_Screens.py
Variables   ../../Screens/Cartoes/Login_Screens.py

*** Variables ***

*** Keywords ***
Acessei a home do app "${instalarToken} com um cpf "${tipo_cliente}" na funcionalidade "${fucnionalidade}"

    ${cpf}=              Carregar dado de conta a partir do json    ${fucnionalidade}   ${tipo_cliente}     cpf
    ${senha_App}=        Carregar dado de conta a partir do json    ${fucnionalidade}   ${tipo_cliente}     senha_app
    Set Test Variable       ${cpf}
    Set Test Variable       ${senha_App}

    Open Application                     http://localhost:4723/wd/hub    app=${EXECDIR}${/}MOBILE${/}app${/}android${/}app-release.apk      platformName=Android      appPackage=com.votorantim.bvpd.uat     appActivity=br.com.votorantim.bvpd.bank.MainActivity    automatationName=Uiautomatior2    autoGrantPermissions=true    newCommandTimeout=10000
    Passar OnBoard
    Sleep                                1
    inserir cpf     ${cpf}
    Sleep                                1
    Clicar no botao continuar
    Inserir senha do App   ${senha_App}
    Clicar no botao continuar
    capture page screenshot
    Pular Biometria

    Run Keyword If      '${instalarToken}' == 'sim'        Instalei o token    ${cpf}
    Run Keyword If      '${instalarToken}' == 'não'        Pular instalação do token


Acesso o menu
    [Arguments]     ${Menu}
#    debug
#    ${valor}=    Run Keyword And Ignore Error     Wait Until Page Contains Element     ${homeBotaoMeusCartoes}
#    Run Keyword If    ${valor}    Clicar no elemento    ${homeBotaoMeusCartoes}
    Run Keyword And Ignore Error        Clicar no elemento    ${homeBotaoMeusCartoes}
#    Clicar no elemento    ${homeBotaoMeusCartoes}