*** Settings ***
Variables   ../../Screens/Cartoes/Login_Screens.py
Variables   ../../Screens/Cartoes/TokenScreen.py
Resource    ../../../Utils/Utils.robot
Resource    ../../PageObjects/Cartoes/Login_Page.robot
Resource    ../../PageObjects/Cartoes/Token_Page.robot


*** Keywords ***
Instalei o token
    [Arguments]    ${cpf}

    Clicar no elemento    ${tokenBotaoInstalarToken}
    ${4_digitos}=       Pegar 4 ultimos digitos do cartao
    ${senha}=           Resgatar senha do cartão senha via API      ${cpf}      ${4_digitos}
    Preecher senha do cartão          ${senha}
    Clicar no elemento                ${btnOkEntendi}
    
Pular instalação do token
    Pular token