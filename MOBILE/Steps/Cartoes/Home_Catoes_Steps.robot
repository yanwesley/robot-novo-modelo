*** Settings ***
Variables   ../../Screens/Cartoes/Login_Screens.py
Variables   ../../Screens/Cartoes/TokenScreen.py
Variables   ../../Screens/Cartoes/HomeMeusCartoes_Screens.py
Resource    ../../../Utils/Utils.robot
Resource    ../../PageObjects/Cartoes/Login_Page.robot
Resource    ../../PageObjects/Cartoes/Token_Page.robot
Resource    ../../Steps/Cartoes/Token_Steps.robot
Resource    ../../PageObjects/Cartoes/HomeCartoes_Page.robot

*** Keywords ***
Gero um cartão virtual
     Clicar no elemento             ${botao_cartao_virtual}
     Instalei o token               ${cpf}
     Clicar no elemento             ${lbl_cartao_titular}
     Clicar no elemento             ${btn_pular_explicao}
     Clicar no elemento             ${btn_gerar_cartao_virtual}
     Inserir Texto em Textfield     ${txt_senha_cartao}    ${SENHA_CARTAO}
     Clicar no elemento             ${btn_continuar}




Clico no botão minhas faturas
    Clicar no elemento    ${botao_minhas_faturas}
    Aguardar Elemento    ${resumo_da_fatura}

Devo visualizar minha fatura fechada
    Wait Until Page Contains Element    ${fatura_fechada}

Acesso a fatura do meio
    Clicar no elemento    ${fatura_meio}

Descartar recebimento de fatura por email

    Clicar no elemento       ${homeCartoesFaturaPorEmailDeixarParaDepois}

Possuo uma fatura fechada
    Possui fatura fechada

clico no botão pagar fatura a partir da home de cartões
    Clicar no botão pagar fatura a partir da home de cartões

Devo visualizar a tela de opções de pagamento
    Validar exibição da tela de opções de pagamento

Possuo uma fatura aberta
    Possui fatura aberta

Acesso a opção de pagamento da fatura com valor total
    Clicar no elemento    ${valor_total}

Acesso a opção de pagamento da fatura com valor minimo
    Clicar no elemento    ${valor_minimo}

Clico em copiar código do boleto
    Clicar no elemento    ${link_codigo_do_boleto}

Devo visualizar a tela com as informações para pagamento do boleto
    Aguardar Elemento    ${lbl_valor_do_boleto}

Cliacar em copiar código de barras
    Clicar no elemento    ${link_codigo_do_boleto}

O link para copiar o código do boleto deve ficar verde para então retornar ao seu estado original
    Aguardar Elemento       ${link_codigo_do_boleto}

Devo visualizar o botão Consultar
    Aguardar Elemento    ${botao_consultar}

Clico no botão ver PDF
    Clicar no elemento    ${botao_ver_pdf}

Devo visualiar a minha fatura fechada em pdf
    Aguardar Elemento    ${botao_compartilhar_pdf}

Devo visualizar mais detalhes da fatura
    Aguardar Elemento    ${}
    Scroll Down    ${Ver_mais}
    Clicar no elemento    ${Ver_mais}
    Aguardar Elemento    ${total_fatura_anterior}
    Aguardar Elemento    ${movimentacao_nacional}
    Aguardar Elemento    ${valor_em_dolar}
    Aguardar Elemento    ${encargos}
    Aguardar Elemento    ${Ajustes}

Devo visualizar a mensagem de sucesso