#Variaves da tela inicial -OnBoard


onBoardSeusServiosBotao = "//android.view.View[@text='Seus serviços BV em um único lugar']"
# onBoardPermitirBotao = "id/permission_allow_foreground_only_button"
onBoardPermitirBotao = "//*[@text='PERMITIR']"
onBoardPermitirBotaoTelefone = "//*[@text='PERMITIR']"

# onBoardPermitirBotaoTelefone = "com.android.permissioncontroller:id/permission_allow_button"
onBoardSetaDireitaBotao = "//android.view.View[@index='5']"
onBoardBotaoVamosComecar = "//android.widget.Button[@text='Vamos começar?']"
botaoMeusCartoes    = "//*[contains(@text,'Meus Cartões')"

botao_Biometria_Deixar_Para_Depois = "//android.view.View[contains(@text,'Deixar para depois')]"
botao_Biometria_Deixar_Ok_Entendi =  "//*[@text='Ok, entendi']"

btn_algo_deu_errado = "//*[@text='Tentar novamente']"
