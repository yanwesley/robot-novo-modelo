informacesGeraisTexto = "//android.view.View[contains(@text, 'Informações gerais')]"

informacesGeraisBotaoCotacaoDolar = """android.widget.ImageView[contains(@text, 'Cotação do dólar
                                       Veja quanto o dólar está valendo')]"""
informacesGeraisBotaoLimitesTaxas = """android.widget.ImageView[contains(@text, 'Limites e taxas
                                        Saiba quais são as condições e limites de crédito')]"""
informacesGeraisBotaoTermosPoliticas = """android.widget.ImageView[contains(@text, 'Termos e políticas
                                        Fique por dentro dos termos de uso e privacidade')]"""
informacesGeraisBotaoTelefones = """android.widget.ImageView[contains(@text, 'Telefones BV
                                 Ligue pra gente em dias úteis, das 8h às 20h')]"""
informacesGeraisBotaoLimitesTaxas = ""
informacesGeraisTexto = ""
