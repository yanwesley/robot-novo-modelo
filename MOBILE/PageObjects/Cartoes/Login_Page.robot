*** Settings ***

Variables   ../../Screens/Cartoes/Login_Screens.py
Resource    ../../../Utils/Utils.robot


*** Variables ***

*** Keywords ***
inserir cpf
    [Arguments]     ${cpf}

    Clicar no elemento                                  ${loginTextoCPFPreliminar}
    Wait Until Page Contains Element                    ${loginTextoCPF}     60
    Inserir Texto em Textfield                         ${loginTextoCPF}    ${cpf}
Clicar no botao continuar
    Clicar no elemento                                   ${loginBotaoContinuar}

Inserir senha do App
    [Arguments]                                     ${senha}

    Wait Until Page Contains Element                ${loginTextoSenha}      30
#    Run Keyword And Return Status                   Clicar no elemento                              //*[contains(@text, 'Acesso inv')]
    Inserir Texto em Textfield                      ${loginTextoSenha}       ${senha}