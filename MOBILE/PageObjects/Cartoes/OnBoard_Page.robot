*** Settings ***
Resource    ../../../Utils/Utils.robot
Resource    ../../PageObjects/Cartoes/Login_Page.robot
Variables   ../../Screens/Cartoes/OnBoard_Screens.py
Variables   ../../Screens/Cartoes/Login_Screens.py
Variables   ../../Screens/Cartoes/HomeMeusCartoes_Screens.py

*** Keywords ***
Passar OnBoard
    Wait Until Page Contains Element     ${onBoardSeusServiosBotao}     60
    sleep                                5
    Scroll Down To Text                  Vamos começar
    Wait Until Page Contains Element     ${onBoardBotaoVamosComecar}        60
    Clicar no elemento                   ${onBoardBotaoVamosComecar}
    Wait Until Page Contains Element     ${loginTextoCPFPreliminar}


Pular Biometria

    Run Keyword And Ignore Error    Clicar no elemento              ${botao_Biometria_Deixar_Para_Depois}
    Run Keyword And Ignore Error    Clicar no elemento              ${botao_Biometria_Deixar_Ok_Entendi}
#    Run Keyword If    ${autorizar} == 'false'     Clicar no elemento               ${btnInstalarToken}

Algo deu errado
    Clicar no elemento    ${btn_algo_deu_errado}

Acesso invalido
    [Arguments]         ${senha_app}
    ${autorizar}=      Run Keyword And Return Status    Wait Until Page Contains Element         ${acesso_invalido}
     Run Keyword And Return Status       Clicar no elemento      "//*[contains(@text, 'Acesso inv')]"  #caso o campo acesso invalido seja exibido
     Run Keyword If         ${autorizar}    Run Keyword And Ignore Error     Inserir senha do App       ${senha_App}
     Run Keyword If         ${autorizar}    Clicar no botao continuar