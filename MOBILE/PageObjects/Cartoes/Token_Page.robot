*** Settings ***
Variables   ../../Screens/Cartoes/TokenScreen.py
Resource    ../../../Utils/Utils.robot



*** Keywords ***
Pegar 4 ultimos digitos do cartao

    ${ultimosDigitos}=      Get Text        ${ultimosDigitos}
    ${ultimosDigitos}=      Get Substring   ${ultimosDigitos}   -4

    [Return]        ${ultimosDigitos}

Prosseguir sem instalar token
#    Set Library Search Order                            AppiumLibrary
    Wait Until Page Contains Element         ${tokenBotaoInstalarToken}         30
    Wait Until Page Contains Element         ${tokenBotaoDeixarParaDepois}      30
    Clicar no elemento                            ${tokenBotaoDeixarParaDepois}
    Wait Until Page Contains Element         ${tokenTextoIrParaTelaInicial}
    Clicar no elemento                            ${tokenTextoIrParaTelaInicial}
    capture page screenshot
#    Wait Until Page Contains Element         ${homeTextSuasContas}

Preecher senha do cartão
    [Arguments]    ${senha}
    Clicar no elemento    ${senhaCartao}
    Inserir Texto em Textfield    ${senhaCartao}    ${senha}
    Wait Until Page Contains Element    ${btnOkEntendi}     60
    
Pular token
    Clicar no elemento    ${btnDeixarParaDepois}
    Clicar no elemento    ${tokenTextoIrParaTelaInicial}