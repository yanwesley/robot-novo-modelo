*** Settings ***

Variables   ../../Screens/Cartoes/AjustesCartao.py
Variables   ../../Screens/Cartoes/InformacoesGerais.py
Resource    ../../../Utils/Utils.robot

*** Keywords ***
Acessar senhas e seguraça
    page should contain element     ${ajusteCartaoTexto}
    Clicar no elemento                   ${ajusteCartaoBotaoInformacoesGerais}
    page should contain element     ${informacesGeraisTexto}