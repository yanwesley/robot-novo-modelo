*** Settings ***
Variables    ../../Screens/Cartoes/HomeMeusCartoes_Screens.py
Variables   ../../Screens/Cartoes/Faturas_Screens.py
Resource    ../../../Utils/Utils.robot


*** Keywords ***
Tocar no botão meus cartões
    page should contain element         ${meusCartoesTexto}
    Clicar no elemento                       ${homeBotaoMeusCartoes}
    Clicar no elemento                       ${meusCartoesMenuHamburger}
    page should contain element         ${ajusteCartaoTexto}

Passar aceite do cartão

    ${aceite_cartao}=   Run Keyword And Return Status       Wait Until Page Contains Element      ${txtEsteSeraSeuNovoCartao}     30
    Run Keyword If      ${aceite_cartao}        Clicar no elemento                       ${X}
    Run Keyword If      ${aceite_cartao}        Clicar no elemento                       ${btnOkEntendi}
    
Validar se a Home de Cartões foi aberta
    
Possui fatura fechada
    Aguardar Elemento    ${fatura_fechada}

Clicar no botão pagar fatura a partir da home de cartões
    Clicar no elemento    ${lblPagarFatura}

Validar exibição da tela de opções de pagamento
    Clicar em Tentar Novamente
    ${status}=    Wait Until Page Contains Element   ${lblOpcoesDePagamento}
    Pass Execution If     $status           Tela com opções de pagamento foi exibida com sucesso

Possui fatura aberta
     Aguardar Elemento    ${fatura_aberta}

