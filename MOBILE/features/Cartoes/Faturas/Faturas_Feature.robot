
*** Settings ***
Library     ${EXECDIR}/LIB/AppiumCustomBV.py
Resource    ../../../PageObjects/Cartoes/AjusteCartoes_Page.robot
Resource    ../../../Steps/Cartoes/Onboard_Steps.robot
Resource    ../../../Steps/Cartoes/Onboard_Steps.robot
Resource    ../../../../MOBILE/Steps/Cartoes/Token_Steps.robot
Resource    ../../../../MOBILE/Steps/Cartoes/Home_Catoes_Steps.robot
Test Setup      Setup

*** Keywords ***

Setup
    Set Library Search Order       Zoomba.MobileLibrary

#Suite setup      Set Global Variable  ${testsRootFolder}  ${EXECDIR}
*** Test Cases ***

Gerar cartão virtual
    [Tags]      robot
    Given Acessei a home do app "sim" com um cpf "fatura_fechada" na funcionalidade "Cartoes"
    And Acesso o menu      "Meus Cartões"
    And gero um cartão virtual
    Then Devo visualizar a mensagem de sucesso

Listar faturas - Fatura fechada
    Given Acessei a home do app "sim" com um cpf "fatura_fechada" na funcionalidade "Cartoes"
    And Acesso o menu      "Meus Cartões"
    And Clico no botão minhas faturas
    And acesso a fatura do meio
    Then Devo visualizar minha fatura fechada

Botão pagar na home quando existe fatura fechada
    Given Acessei a home do app "não" com um cpf "jose" na funcionalidade "Cartoes"
    And Acesso o menu      "Meus Cartões"
    And Possuo uma fatura fechada
    When clico no botão pagar fatura a partir da home de cartões
    Then devo visualizar a tela de opções de pagamento

Botão pagar na home quando existe fatura aberta
    Given Acessei a home do app "não" com um cpf "fatura_aberta" na funcionalidade "fatura"
    And Acesso o menu      "Meus Cartões"
    And possuo uma fatura aberta
    Then Devo visualizar o botão Consultar

Faturas - Copiar código do boleto - fatura_fechada_paga_total
    Given Acessei a home do app "não" com um cpf "fatura_fechada_paga_total" na funcionalidade "fatura"
    And Acesso o menu      "Meus Cartões"
    And clico no botão pagar fatura a partir da home de cartões
    And acesso a opção de pagamento da fatura com valor total
    When clico em copiar código do boleto
    Then o link para copiar o código do boleto deve ficar verde para então retornar ao seu estado original
#    Necessario corrigir esse metodo para validar a troca de cor/text do link copiar boleto

Faturas - Pagamento de fatura com valor minimo
    [Tags]       robot123     fatura         @CSA-T154
        Given Acessei a home do app "não" com um cpf "valor_minimo" na funcionalidade "fatura"
        And Acesso o menu      "Meus Cartões"
        And clico no botão pagar fatura a partir da home de cartões
        And acesso a opção de pagamento da fatura com valor minimo
        Then devo visualizar a tela com as informações para pagamento do boleto

Faturas - Pagamento de fatura com valor total
    Given Acessei a home do app "não" com um cpf "valor_minimo" na funcionalidade "fatura"
    And Acesso o menu      "Meus Cartões"
    And clico no botão pagar fatura a partir da home de cartões
    And acesso a opção de pagamento da fatura com valor total
    Then devo visualizar a tela com as informações para pagamento do boleto

Faturas - Visualizar Boleto em PDF
    Given Acessei a home do app "não" com um cpf "valor_minimo" na funcionalidade "fatura"
    And Acesso o menu      "Meus Cartões"
    And clico no botão pagar fatura a partir da home de cartões
    And acesso a opção de pagamento da fatura com valor total
    And clico no botão ver PDF
    Pass Execution If    Then devo visualiar a minha fatura fechada em pdf    Teste executado com sucesso

Consultar ver mais faturas
    Given Acessei a home do app "não" com um cpf "valor_minimo" na funcionalidade "fatura"
    And Acesso o menu      "Meus Cartões"
    And Clico no botão minhas faturas
    Then devo visualizar mais detalhes da fatura