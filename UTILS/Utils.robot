*** Settings ***
Library    Zoomba.MobileLibrary
Library    BuiltIn
Library    OperatingSystem
Library    String
Library    BuiltIn
Library    JSONLibrary
Library    DebugLibrary
Library    Screenshot
Library    RequestsLibrary
Library    ../LIB/AppiumCustomBV.py

*** Keywords ***
#Inserir Texto em Textfield
#    [Arguments]                             ${field}    ${value}
#    Wait Until Page Contains Element        ${field}
#
#    ${tamanhoString} =                      Get Length      ${value}
#
#    FOR    ${i}    IN RANGE    ${tamanhoString}
#
#           Exit For Loop If             ${i} == ${tamanhoString}
#
#           ${tamanhoSubString} =        Evaluate    ${i}+1
#           ${textoChar} =               Get Substring          ${value}      ${i}        ${tamanhoSubString}
#           run                          adb shell input text ${textoChar}
#           sleep                        1
#    END
#
#    log TO CONSOLE                      Valor ${value} inserido no campo ${field}

Clicar no elemento
    [Arguments]                         ${elemento}     ${timeout}=30
    Wait Until Page Contains Element    ${elemento}     ${timeout}      O elemento ${elemento} não apareceu em ${timeout} segundos
    Click Element                       ${elemento}
    Capture Page Screenshot


Aguardar Elemento
    [Arguments]                         ${elemento}     ${timeout}=30
    Wait Until Page Contains Element    ${elemento}     ${timeout}      O elemento ${elemento} não foi exibido dentro de ${timeout} segundos

Clicar em Tentar Novamente
    ${status}=    Run Keyword And Return Status     Wait For And Click Element    "//*[contains(@label, 'Tentar novamente')]"        30


Executar metodo get
    [Arguments]     ${url_end_point}    ${parametros}   ${expect_Status}=any   ${mensagem}=None     ${kwargs}=None


    ${response}=   Run Keyword If    ${expect_Status}==any
    ...   GET     ${url_end_point}   ${parametros}    verify=${False}
    ...   ELSE    GET    ${url_end_point}   ${parametros}    ${expect_Status}   ${mensagem}   verify=${False}

    [Return]    ${response}



Carregar dado de conta a partir do json
    [Arguments]     ${funcionalidade}      ${tipo_cliente}      ${dado}

#    caminho relativo do arquivo json

    &{dict}=   Load JSON From File   ${EXECDIR}/MASSA/${funcionalidade}/Massa.json

    [Return]   ${dict}[${tipo_cliente}][0][${dado}]

Resgatar senha do cartão senha via API
    [Arguments]          ${cpf}         ${cartao}       ${expected_status}=200
    &{dict}=                Create Dictionary     cpf=${cpf}
    ${mensage_erro_api}     Set Variable         Erro ao resgatar a senha via API
    ${URL_API}              Set Variable         https://sboot-cart-base-orch-cartoes-senha-poc.appdes.bvnet.bv/v1/banco-digital/cartoes/pin/

    ${reposta}=     Executar metodo get  ${URL_API}     ${dict}     ${expected_status}      ${mensage_erro_api}

#    retorna a quantidade de itens no array
    ${qtd_cartoes}    Get Length    ${reposta.json()}[listaCartao]

    FOR    ${i}    IN RANGE    ${qtd_cartoes}
            ${SENHA_CARTAO}     Set Variable   ${reposta.json()}[listaCartao][${i}][pin]
            Set Test Variable           ${SENHA_CARTAO}
           Exit For Loop If             ${reposta.json()}[listaCartao][${i}][pin] == ${cartao}
    END
    [Return]     ${SENHA_CARTAO}